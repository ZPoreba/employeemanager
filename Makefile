all: compile run

compile:
	javac --add-modules=java.xml.ws.annotation --add-modules=java.xml.bind sample/CreateGroup.java sample/ExistingGroup.java sample/Main.java sample/MainController.java sample/xmlManager.java 2>> error_logs.log 1>>app_logs.log
run:
	java --add-modules=java.xml.ws.annotation --add-modules=java.xml.bind sample.Main  2>> error_logs.log 1>> app_logs.log
clean:
	rm -f sample/*.class app_logs.log error_logs.log

generate_javadoc:
	javadoc -private --add-modules=java.xml.ws.annotation --add-modules=java.xml.bind sample -d MainDocumentation

clean_javadoc:
	rm -rf MainDocumentation
