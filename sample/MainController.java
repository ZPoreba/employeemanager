package sample;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Klasa zarządza głównym widokiem. W zależności od wybranej przez użytkownika opcji zmienia widok na odpowiedni ładując
 * plik fxml odpowiadający danej opcji. Reszta działań dla poszczególnych widoków jest realizowana w osobnych kontrolerach.
 */

public class MainController {

    @FXML
    GridPane pane;

    static List fileList;

/**
 * {@link MainController#fileList}
 * Metoda zmienia widok poprzez załadowanie odpowiedniego pliku fxml w zależności od wyboru dokonanego przez użytkownika.
 *
 * @param fileName Nazwa pliku fxml
 */
    public void changeScreen(String fileName){
        try {
            pane.getScene().setRoot(FXMLLoader.load(getClass().getResource(fileName)));
        }
        catch(IOException e){
            System.out.println("Exception during changing screen: " + e.getMessage() + " ");
            e.printStackTrace();
        }
    }

/**
 * Metoda wykonuje akcje specyficzne dla dokonanego wyboru, czyli dla wyboru otworzenia istniejącej bazy pracowniczej.
 * Wyświetla okno wyboru. Następnie użytkownik powinien wybrać folder z wcześniej utworzoną bazą. Po dokonaniu wyboru
 * użytkownik zostanie przekierowany do widoku umożliwiającego zarządzanie wcześniej utworzoną bazą.
 */
    public void existingScreen() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Open Resource File");
        File chosenDir = directoryChooser.showDialog(pane.getScene().getWindow());

        if (chosenDir != null){
            this.fileList = Arrays.asList(chosenDir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {

                    return name.endsWith(".xml");
                }
            }));

            changeScreen("/fxmlFiles/existingGroup.fxml");
        }
    }

/**
 * Metoda statyczna umożliwiająca dostęp do listy plików xml klasom zewnętrznym.
 *
 * @return Lista plików xml z wszystkimi pracownikami istniejącymi w bazie. 
 */
    public static List getFileList(){
        return fileList;
    }

/**
 * Metoda wykonuje akcje specyficzne dla dokonanego wyboru, czyli dla wyboru utworzenia nowej bazy pracowniczej.
 * Wyświetla okno wybotu. Następnie użytkownik powinien wybrać folder w którym zostanie utworzon baza. Po dokonaniu
 * wyboru użytkownik zostanie przekierowany do widoku umożliwiającego stworzenie nowej bazy.
 */
    public void creatingScreen() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Choose path where directory will be created");
        xmlManager.dirPath = directoryChooser.showDialog(pane.getScene().getWindow());

        if (xmlManager.dirPath != null) changeScreen("/fxmlFiles/createGroup.fxml");

    }
}
