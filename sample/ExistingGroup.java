package sample;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Klasa kontrolująca widok po wybraniu istaniejącej bazy. Użytkownik po otworzeniu wcześniej stworzonej bazy pracowniczej może
 * otworzyć ją. Po wybraniu takiej opcji obsługą widoku zajmuje się klasa ExistingGroup.
 */
public class ExistingGroup implements Initializable {

    @FXML
    ListView listViewE;

    @FXML
    TextField name;

    @FXML
    TextField surname;

    @FXML
    TextField age;

    @FXML
    TextField id;

    @FXML
    TextField manager;

    @FXML
    TextField position;

    @FXML
    TextField phone;

    @FXML
    TextField email;

    List<File> xmlFiles;

    @FXML
    TextField fieldSearch;

    @FXML
    Button buttonSearch;

    @FXML
    ComboBox comboSearch;

    @FXML
    Button showAllButton;

    @FXML
    AnchorPane pane;

    @FXML
    Button mon;

    @FXML
    Button tue;

    @FXML
    Button wed;

    @FXML
    Button thu;

    @FXML
    Button fri;

    @FXML
    Button sun;

    @FXML
    Button sat;

    List<String> workingDays = new ArrayList<String>();

/**
 * Konstruktor pobiera listę plików znajdującą się w katalogu wybranym przez użytkownika.
 */
    public ExistingGroup(){
        xmlFiles = MainController.getFileList();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb){
        loadData();
    }

/**
 * Metoda wyświetla użytkowników pobranych w konstruktorze w formie listy.
 */
    private void loadData(){
        for(int i =0; i<xmlFiles.size(); i++){
            Employee employee = xmlManager.xmlToObject(xmlFiles.get(i));
            Text listElement = new Text(employee.getName() + " " + employee.getSurname());
            listElement.setStyle("-fx-fill: white; -fx-font: 17px Helvetica;");
            listViewE.getItems().add(listElement);
        }
    }

/**
 * Metoda ustawia dni pracy danego pracownika w bazie na podstawie dostarczonej listy. 
 *
 * @param  list Lista z określonymi dniami pracy danego pracownika istniejącego w bazie.
 */
    public void setWorkingDaysVisible(List<String> list){

        String orangeStyle = "-fx-background-color: " +
                "linear-gradient(from 0% 93% to 0% 100%, #a34313 0%, #903b12 100%),\n" +
                "#9d4024,\n" +
                "#d86e3a,\n" +
                "radial-gradient(center 50% 50%, radius 100%, #d86e3a, #c54e2c);";

        String blueStyle = "-fx-background-color: rgb(0, 153, 122)";

        mon.setStyle(orangeStyle);
        tue.setStyle(orangeStyle);
        wed.setStyle(orangeStyle);
        thu.setStyle(orangeStyle);
        fri.setStyle(orangeStyle);
        sat.setStyle(orangeStyle);
        sun.setStyle(orangeStyle);

        if(list != null) {
            for (String s : list) {
                if (s.equals("mon")) mon.setStyle(blueStyle);
                if (s.equals("tue")) tue.setStyle(blueStyle);
                if (s.equals("wed")) wed.setStyle(blueStyle);
                if (s.equals("thu")) thu.setStyle(blueStyle);
                if (s.equals("fri")) fri.setStyle(blueStyle);
                if (s.equals("sat")) sat.setStyle(blueStyle);
                if (s.equals("sun")) sun.setStyle(blueStyle);
            }
        }
    }

/**
 * Metoda wyświetla informacje na temat pracownika zapisane w pliku xml opisującym daną osobę.
 *
 * @param event Metoda jest wywoływana przez kliknięcie w użytkownika na liście, dlatego niezbędny jest,
 * parametr określający dany event który obsługujemy. 
 */
    public void displayEmployee(MouseEvent event){
        int selectedIndex = listViewE.getSelectionModel().getSelectedIndex();
        Employee employee = xmlManager.xmlToObject(xmlFiles.get(selectedIndex));

        this.name.setText(employee.getName());
        this.surname.setText(employee.getSurname());
        this.age.setText(Integer.toString(employee.getAge()));
        this.id.setText(Integer.toString(employee.getId()));
        this.manager.setText(employee.getManager());
        this.position.setText(employee.getPosition());
        this.phone.setText(employee.getPhone());
        this.email.setText(employee.getEmail());
        workingDays = employee.getWorkingDays().getDays();
        this.setWorkingDaysVisible(workingDays);

    }

/**
 * Metoda zmienia widok na widok głowny. Używana podczas powrotu do menu głównego.
 */
    public void changeScreen(){
        try {
            pane.getScene().setRoot(FXMLLoader.load(getClass().getResource("../fxmlFiles/sample.fxml")));
        }
        catch(IOException e){
            System.out.println("Exception during changing screen: " + e.getMessage() + " ");
            e.printStackTrace();
        }
    }

/**
 * Metoda ustawia widoczność pól służących do wyszukiwania użytkowników względem określonych informacji.
 */
    public void searchEmployee(){
        fieldSearch.visibleProperty().setValue(true);
        buttonSearch.visibleProperty().setValue(true);
        comboSearch.visibleProperty().setValue(true);
        buttonSearch.defaultButtonProperty().setValue(true);
    }

/**
 * Metoda wykonuje procedurę wyszukiwania. Czyści pola z poprzednio załadowanych wartości, ustawia widoczny przycisk showAll,
 * wywołuje metodę find zwracającą listę użytkowników pasujących do poszukiwanych kryteriów, ustawia użytkowników na liście.
 */
    public void startSearching(){
        this.clearTextFields();
        showAllButton.visibleProperty().setValue(true);
        int selectedIndex = comboSearch.getSelectionModel().getSelectedIndex();
        xmlFiles = xmlManager.find(xmlFiles, fieldSearch.getText(), selectedIndex);
        listViewE.getItems().clear();
        loadData();
    }

/**
 * Metoda czyści pola z informacjami o użytkowniku oraz ustawia listę dni pracujących na listę pustą.
 * Czyści widok z wcześniej wyświetlonych danych. 
 */
    private void clearTextFields(){
        this.name.clear();
        this.surname.clear();
        this.age.clear();
        this.id.clear();
        this.manager.clear();
        this.position.clear();
        this.phone.clear();
        this.email.clear();

        workingDays = new ArrayList<>();
        setWorkingDaysVisible(workingDays);
    }

/**
 * Metoda przywraca widok wszystkich użytkowników na liście. Po wyszukaniu lista użytkowników jest okrojona do
 * użytkowników spełniających wymagania kryteriów wyszukiwania. Dana metoda przywraca wyjściową listę użytkowników
 * czyli listę z wszystkimi użytkownikami istniejącymi w bazie.
 */
    public void showAllMethod(){
        xmlFiles = MainController.getFileList();
        listViewE.getItems().clear();
        loadData();
        this.clearTextFields();
    }

}
