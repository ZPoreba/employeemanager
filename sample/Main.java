package sample;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * Klasa uruchamia całą aplikacje.
 */

public class Main extends Application {

/**
 * Metoda start uruchamia aplikacje, ładuje plik fxml w którym opisany jest wygląd widoku
 * głownego. Nastepnie ustawiane są poszczególne parametry okna oraz ustawiany plik css z 
 * klasami stylizujacymi konkretne komponenty. 
 *
 * @param  primaryStage  Scena aplikacji, najwyższy kontener w ktorym znajdują sie pozostałe
 * komponenty
 */

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../fxmlFiles/sample.fxml"));
        primaryStage.setTitle("Employee Manager");
        Scene scene = new Scene(root, 1300, 800);
        scene.getStylesheets().add("cssFiles/appStyle.css");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
