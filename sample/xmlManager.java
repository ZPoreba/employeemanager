package sample;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.*;
import javax.xml.bind.annotation.*;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.*;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;
import java.io.IOException;

/**
 * Klasa obiektów na które mapujemy dni tygodnia poszczególnego pracownika. Osobna klasa zapewnia możliwość
 * utworzenia elementu workingDays który zawiera określoną ilość elementów day. 
 */
class WorkingDays
{
    List<String> day = new ArrayList<String>();

    @XmlElement(name="day")
    public List<String> getDays() { return day; }
    public void setDays(List<String> day) { this.day = day; }
}

/**
 * Klasa obiektów na które mapujemy plik xml. Umożliwia stworzenie obiektu na podstawie własnych parametrów
 * oraz mapowanie istaniejącego pliku xml o odpowiedniej strukturze na obiekt.
 */
@XmlRootElement(name="employee")
class Employee {

    String name;
    String surname;
    int age;
    int id;
    String manager;
    String position;
    String phone;
    String email;
    WorkingDays data = new WorkingDays();

    public String getName() {
        return name;
    }

    @XmlElement(name="name")
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname(){
        return surname;
    }

    @XmlElement(name="surname")
    public void setSurname(String surname){
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    @XmlElement(name="age")
    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    @XmlElement(name="id")
    public void setId(int id ){ this.id = id; }

    public String getManager(){
        return manager;
    }

    @XmlElement(name="manager")
    public void setManager(String manager){
        this.manager = manager;
    }

    public String getPosition(){
        return position;
    }

    @XmlElement(name="position")
    public void setPosition(String position){
        this.position = position;
    }

    public String getPhone(){
        return phone;
    }

    @XmlElement(name="phone")
    public void setPhone(String phone){
        this.phone = phone;
    }

    public String getEmail(){
        return email;
    }

    @XmlElement(name="email")
    public void setEmail(String email){
        this.email = email;
    }

    public WorkingDays getWorkingDays(){ return this.data; }

    @XmlElement(name="workingDays")
    public void setWorkingDays(WorkingDays data){ this.data = data; }
}

/**
 * Klasa zapewnia obsługę plików xml. Tworzenie plików na podstawie obiektów, mapowanie plików na obiekty, validacje
 * plików za pomocą xmlSchema. 
 */

public class xmlManager {

    public static int employeeIdCount=0;

    public static File dirPath;

/**
 * Metoda tworzy plik xml o podanej nazwie w wybranym przez użytkownika miejscu. 
 *
 * @param  name Nazwa tworzonego pliku.
 *
 * @return Ścieżka do pliku xml z danymi pracownika
 */
    private static File createFile(String name) {
        String employeePath = dirPath.getAbsolutePath() + "/tmp_employees";

        if (!Files.isDirectory(Paths.get(employeePath))) new File(employeePath).mkdirs();
        return new File(employeePath + "/" + name + "_" + employeeIdCount + ".xml");
    }


/**
 * Metoda zwraca scieżkę bezpośrednią folderu z bazą pracowników.
 *
 * @return ścieżka bezpośrednia 
 */
    public static File getMainDirPath(){
        return new File(dirPath.getAbsolutePath() + "/tmp_employees");
    }

/**
 * Metoda zmienia obiekt w plik xml reprezentujący pracownika. Podajemy konkretne informacje na temat pracownika
 * następnie metoda zmienia informacje w plik xml.
 *
 * @param name Imię pracownika
 * @param surname Nazwisko pracownika
 * @param age Wiek pracownika
 * @param manager Menadżer pracownika
 * @param position Pozycja pracownika
 * @param phone Numer telefonu pracownika
 * @param email Email pracownika
 * @param workingDays Dni pracujące pracownika
 *
 * @return Plik z obiektem reprezentującym pracownika w formacie xml
 */
    public static File objectsToXml(String name, String surname, int age, String manager, String position, String phone, String email, List<String> workingDays) {
        try {
            System.out.println(surname);
            System.out.println(position);

            Employee p = new Employee();

            p.setName(name);
            p.setSurname(surname);
            p.setAge(age);
            p.setId(employeeIdCount);
            p.setManager(manager);
            p.setPosition(position);
            p.setPhone(phone);
            p.setEmail(email);

            WorkingDays days = new WorkingDays();
            for(int i =0; i<workingDays.size(); i++) days.getDays().add(workingDays.get(i));
            p.setWorkingDays(days);

            File f = createFile(name + "_" + surname);
            JAXBContext ctx = JAXBContext.newInstance(Employee.class);
            Marshaller marshaller = ctx.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, "xmlSchema.xsd");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(p, System.out);
            marshaller.marshal(p, f);

            return f;
        }
        catch (JAXBException e){
            System.out.println("JAXB exception - objectsToXml: " + e.getMessage());
        }
        return null;
    }

/**
 * Metoda służy do walidacji pliku xml przy pomocy wcześniej zdefiniowanego pliku xmlSchema.
 *
 * @param  f Plik xml z danymi pracownika, który zostanie poddany walidacji.
 */
    public static void xmlSchemaValidation(File f){

        Source xmlFile = new StreamSource(f);
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        File schemaFile = new File("sample/xmlSchema.xsd");

        try {
            Schema schema = schemaFactory.newSchema(schemaFile);
            Validator validator = schema.newValidator();
            validator.validate(xmlFile);
            System.out.println(xmlFile.getSystemId() + " is valid");
        } catch (SAXException e) {
            System.out.println(xmlFile.getSystemId() + " is NOT valid reason:" + e);
        } catch (IOException e) {
            System.out.println("Problem with file:" + e);
        }

    }

/**
 * Metoda rzutuje plik xml na obiekt klasy Employee.
 *
 * @param f Plik xml z danymi na temat pracownika.
 *
 * @return Obiekt klasy Employee stworzony na podstawie pliku xml.
 */
    public static Employee xmlToObject(File f){

        Employee p;
        try {
            JAXBContext ctx = JAXBContext.newInstance(Employee.class);
            Unmarshaller unmarshaller = ctx.createUnmarshaller();
            xmlSchemaValidation(f);
            p = (Employee) unmarshaller.unmarshal(f);

            return p;
        }
        catch(JAXBException e){
            System.out.println("JAXB exception - xmlToObject: " + e.getMessage());
        }
        return null;
    }

/**
 * Metoda tworzy listę z pracownikami odpowiadającymi zadanym kryteriom wyszukiwania. Jest wykorzystywana przy wyszukiwaniu
 * pracowników. Użytkownik wybiera kategorię według której chce szukać, następnie wpisuję frazę i następuje wyszukiwanie. 
 *
 * @param fileList Lista pracowników wśród których nastąpi wyszukiwanie.
 * @param element Fraza wpisana przez użytkownika.
 * @param number Numer porządkowy odpowiadający wyszukiwaniu po konkretnym elemencie pliku xml.
 *
 * @return Lista pracowników zgodnych z określonymi kryteriami.
 */
    public static ArrayList<File> find(List<File> fileList, String element, int number){
        ArrayList<File> foundFiles = new ArrayList<>();

        for(int i = 0; i<fileList.size(); i++){

            if ( number == 0 && xmlToObject(fileList.get(i)).getName().equals(element) ) {
                foundFiles.add(fileList.get(i));
            }

            if ( number == 1 && xmlToObject(fileList.get(i)).getSurname().equals(element) ){
                foundFiles.add(fileList.get(i));
            }

            if ( number == 2 && xmlToObject(fileList.get(i)).getAge() == Integer.parseInt(element)) {
                foundFiles.add(fileList.get(i));
            }

            if ( number == 3 && xmlToObject(fileList.get(i)).getId() == Integer.parseInt(element)) {
                foundFiles.add(fileList.get(i));
            }

            if ( number == 4 && xmlToObject(fileList.get(i)).getManager().equals(element) ) {
                foundFiles.add(fileList.get(i));
            }

            if ( number == 5 && xmlToObject(fileList.get(i)).getPosition().equals(element) ) {
                foundFiles.add(fileList.get(i));
            }

            if ( number == 6 && xmlToObject(fileList.get(i)).getPhone().equals(element) ) {
                foundFiles.add(fileList.get(i));
            }

            if ( number == 7 && xmlToObject(fileList.get(i)).getEmail().equals(element) ) {
                foundFiles.add(fileList.get(i));
            }

            }

            return foundFiles;
        }
}
