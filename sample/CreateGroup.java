package sample;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasa kontrolująca widok po wybraniu opcji tworzenia nowej bazy. Użytkownik po wybraniu opcji może tworzyć baze praconików, usuwać i edytować
 * wprowadzonych pracowników. 
 */
public class CreateGroup {

    @FXML
    ListView listView;

    @FXML
    TextField name;

    @FXML
    TextField surname;

    @FXML
    TextField age;

    @FXML
    TextField manager;

    @FXML
    TextField position;

    @FXML
    TextField phone;

    @FXML
    TextField email;

    @FXML
    AnchorPane pane;

    @FXML
    Button eButton;

    @FXML
    Button mon;

    @FXML
    Button tue;

    @FXML
    Button wed;

    @FXML
    Button thu;

    @FXML
    Button fri;

    @FXML
    Button sun;

    @FXML
    Button sat;

    @FXML
    Text fieldWarning;

    List<String> workingDays = new ArrayList<String>();
    String pickedDay;
    ArrayList<File> xmlFiles = new ArrayList<>();

/**
 * Metoda czyści pola z informacjami o użytkowniku oraz ustawia listę dni pracujących na listę pustą.
 * Czyści widok z wcześniej wyświetlonych danych. 
 */
    private void clearTextFields(){
        this.name.clear();
        this.surname.clear();
        this.age.clear();
        this.manager.clear();
        this.position.clear();
        this.phone.clear();
        this.email.clear();

        workingDays = new ArrayList<String>();
        setWorkingDaysVisible(workingDays);
    }

/**
 * Metoda dodaje nowego użytkownika do bazy pracowniczej na podstawie informacji wprowadzonych w pola. Tworzony jest plik xml 
 * z danymi na tamat pracownika. Pola name, surname, age, phone muszą być wypełnione ponieważ są to podstawowe, minimalne informacje.
 * reszta pól jest opcjonalna. Pracownik wprowadzany jest na listę i zostaje stworzony plik xml z jego danymi.
 */  
    public void addNewEmployee() {
        xmlManager.employeeIdCount++;
        fieldWarning.setVisible(false);

        if( !name.getText().equals("") && !surname.getText().equals("") && !age.getText().equals("") && !phone.getText().equals("") ) {
            try {
                String employeeName = name.getText();
                String employeeSurname = surname.getText();
                int employeeAge = Integer.parseInt(age.getText());
                String employeeManager = manager.getText();
                String employeePosition = position.getText();
                String employeePhone = phone.getText();
                String employeeEmail = email.getText();

                Text listElement = new Text(employeeName + " " + employeeSurname);
                listElement.setStyle("-fx-fill: white; -fx-font: 17px Helvetica;");
                listView.getItems().add(listElement);
                File f = xmlManager.objectsToXml(employeeName, employeeSurname, employeeAge, employeeManager, employeePosition, employeePhone, employeeEmail, this.workingDays);
                xmlFiles.add(f);

                this.clearTextFields();
            }
            catch(NumberFormatException e){
                fieldWarning.setText("Wrong value in field age! Input only numbers in this field!");
                fieldWarning.setVisible(true);
            }
        }
        else{
            fieldWarning.setText("You have to fill at least filed with name, surname, age and phone!");
            fieldWarning.setVisible(true);
        }
    }

/**
 * Metoda zmienia widok na widok głowny. Używana podczas powrotu do menu głównego.
 */
    public void changeScreen(){
        try {
            pane.getScene().setRoot(FXMLLoader.load(getClass().getResource("../fxmlFiles/sample.fxml")));
        }
        catch(IOException e){
            System.out.println("Exception during changing screen: " + e.getMessage() + " ");
            e.printStackTrace();
        }
    }

/**
 * Metoda wyświetla okno dialogowe wymagające potwierdzenia przez użytkownika. Po wybraniu przez użytkownika opcji "Quit without saving"
 * wywoływana jest ta metoda. Następnie w zależności od wyboru użytkonika następuje przejście do menu głownego bez zapisania bazy lub 
 * powrót do panelu edycji. 
 */
    public void quitWithoutSaving(){
        xmlManager.employeeIdCount=0; // zeruje licznik id (w przypadku tworzenia nowej grupy znow id od 1)

        Object[] options = {"Yes", "No"};

        int n = JOptionPane.showOptionDialog(null,
                "Are you sure that you want to quit without saving ?",
                "Quit without saving",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0]);

        if (n == 0){

            File deletingFolder = xmlManager.getMainDirPath();
            final File[] files = deletingFolder.listFiles();
            if (files != null) for (File f: files) f.delete();
            deletingFolder.delete();

            changeScreen();
        }
    }

/**
 * Metoda wyświetla okno dialogowe wymagające potwierdzenia przez użytkownika. Po wybraniu przez użytkownika opcji "Save and quit"
 * wywoływana jest ta metoda. Następnie w zależności od wyboru użytkownika baza jest zapisywana lub następuje powrót do panelu edycji.
 */
    public void saveAndQuit(){

        String s = (String)JOptionPane.showInputDialog(null,
                "You chose path " + xmlManager.dirPath + "\n Input name of created directory:\n",
                "Save and quit",
                JOptionPane.PLAIN_MESSAGE,
                null,
                null, "");

        if ((s != null) && (s.length() > 0)) {
            File oldName = new File(xmlManager.dirPath + "/tmp_employees");
            File newName = new File(xmlManager.dirPath + "/" + s);
            oldName.renameTo(newName);
            changeScreen();
            return;
        }

    }

/**
 * Metoda ustawia dni pracy danego pracownika w bazie na podstawie dostarczonej listy. 
 *
 * @param  list Lista z określonymi dniami pracy danego pracownika istniejącego w bazie.
 */    
    public void setWorkingDaysVisible(List<String> list){

        String orangeStyle = "-fx-background-color: " +
                "linear-gradient(from 0% 93% to 0% 100%, #a34313 0%, #903b12 100%),\n" +
                "#9d4024,\n" +
                "#d86e3a,\n" +
                "radial-gradient(center 50% 50%, radius 100%, #d86e3a, #c54e2c);";

        String blueStyle = "-fx-background-color: rgb(0, 153, 122)";

        mon.setStyle(orangeStyle);
        tue.setStyle(orangeStyle);
        wed.setStyle(orangeStyle);
        thu.setStyle(orangeStyle);
        fri.setStyle(orangeStyle);
        sat.setStyle(orangeStyle);
        sun.setStyle(orangeStyle);


        if(list != null) {
            for (String s : list) {
                if (s.equals("mon")) mon.setStyle(blueStyle);
                if (s.equals("tue")) tue.setStyle(blueStyle);
                if (s.equals("wed")) wed.setStyle(blueStyle);
                if (s.equals("thu")) thu.setStyle(blueStyle);
                if (s.equals("fri")) fri.setStyle(blueStyle);
                if (s.equals("sat")) sat.setStyle(blueStyle);
                if (s.equals("sun")) sun.setStyle(blueStyle);
            }
        }
    }
 
/**
 * Metoda wyświetla informacje na temat pracownika zapisane w pliku xml opisującym daną osobę.
 *
 * @param event Metoda jest wywoływana przez kliknięcie w użytkownika na liście, dlatego niezbędny jest,
 * parametr określający dany event który obsługujemy. 
 */   
    public void employeePicked(MouseEvent event){

        eButton.visibleProperty().setValue(true);

        int selectedIndex = listView.getSelectionModel().getSelectedIndex();
        Employee employee = xmlManager.xmlToObject(xmlFiles.get(selectedIndex));

        this.name.setText(employee.getName());
        this.surname.setText(employee.getSurname());
        this.age.setText(Integer.toString(employee.getAge()));
        this.manager.setText(employee.getManager());
        this.position.setText(employee.getPosition());
        this.phone.setText(employee.getPhone());
        this.email.setText(employee.getEmail());
        workingDays = employee.getWorkingDays().getDays();
        this.setWorkingDaysVisible(workingDays);
    }

/**
 * Metoda wyświetla informacje na temat pracownika oraz umożliwia edycje wczesniej wprowadzonych danych.
 */
    public void editButtonClicked(){
        int selectedIndex = listView.getSelectionModel().getSelectedIndex();
        String employeeName = this.name.getText();
        String employeeSurname = this.surname.getText();
        int employeeAge = Integer.parseInt(this.age.getText());
        String employeeManager = manager.getText();
        String employeePosition = position.getText();
        String employeePhone = phone.getText();
        String employeeEmail = email.getText();

        xmlFiles.get(selectedIndex).delete();
        File xmlEditedFile = xmlManager.objectsToXml(employeeName, employeeSurname, employeeAge, employeeManager, employeePosition, employeePhone, employeeEmail, this.workingDays);
        xmlFiles.set(selectedIndex, xmlEditedFile);

        Text listElement = new Text(employeeName + " " + employeeSurname);
        listElement.setStyle("-fx-fill: white; -fx-font: 17px Helvetica;");
        listView.getItems().set(selectedIndex, listElement);

        eButton.visibleProperty().setValue(false);
        this.clearTextFields();
    }

/**
 * Metoda umożliwia usunięcie wcześniej stworzonego pracownika. 
 */
    public void deleteNewEmployee(){
        int selectedIndex = listView.getSelectionModel().getSelectedIndex();
        xmlFiles.get(selectedIndex).delete();
        xmlFiles.remove(selectedIndex);
        listView.getItems().remove(selectedIndex);

        this.clearTextFields();
    }


    public void monPicked(){
        this.pickedDay = "mon";
        weekPicker(mon);
    }

    public void tuePicked(){
        this.pickedDay = "tue";
        weekPicker(tue);
    }

    public void wedPicked(){
        this.pickedDay = "wed";
        weekPicker(wed);
    }

    public void thuPicked(){
        this.pickedDay = "thu";
        weekPicker(thu);
    }

    public void friPicked(){
        this.pickedDay = "fri";
        weekPicker(fri);
    }

    public void satPicked(){
        this.pickedDay = "sat";
        weekPicker(sat);
    }

    public void sunPicked(){
        this.pickedDay = "sun";
        weekPicker(sun);
    }

/**
 * Metoda tworzy listę dni pracujących dla danego pracownika w zależności od zaznaczonych opcji.
 *
 * @param dayButton Dla każdego dnia tygodnia mamy osobny przycisk, w zależności od wyboru podświetlany jest odpowiedni z nich. Zmienna dayButton
 * umożliwia wprowadzenie dnia tygodnia po wyborze dokonanym przez użytkownika.  
 */
    public void weekPicker(Button dayButton){
        String orangeStyle = "-fx-background-color: " +
                "linear-gradient(from 0% 93% to 0% 100%, #a34313 0%, #903b12 100%),\n" +
                "#9d4024,\n" +
                "#d86e3a,\n" +
                "radial-gradient(center 50% 50%, radius 100%, #d86e3a, #c54e2c);";

        String blueStyle = "-fx-background-color: rgb(0, 153, 122)";

        if (dayButton.getStyle().equals(blueStyle)){
            dayButton.setStyle(orangeStyle);
            workingDays.remove(pickedDay);
        }
        else{
            dayButton.setStyle(blueStyle);
            workingDays.add(pickedDay);
        }
    }

}
